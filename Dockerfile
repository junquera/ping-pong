FROM ubuntu

RUN apt update && apt install -y netcat


COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT /entrypoint.sh
