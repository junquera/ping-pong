#!/bin/bash
set -e

play (){
	sleep 1;
        echo $NAME | nc -q 0 $ADVERSARY 1337;
}

NAME="pong"
if [ $STARTER ]; then
	NAME="ping"
	play
fi;


while true; do
	nc -l -p 1337
	play
done
